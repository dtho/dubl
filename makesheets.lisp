(in-package :dubl)
;;
;; generate OMR sheet (a "bubble sheet")
;;
(defparameter *id-row-bubble-labels*
  '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))

(defparameter *single-char-answer-designators*
  '(#\A #\B #\C #\D #\E #\F #\G #\H)
  "The default single-character answer designators.")

;; PAGE holds dimensions of various page components
(defclass page ()
  ((bottom-margin
    :accessor page-bottom-margin
    :documentation "A measure representing the bottom margin of the
    page. We define the margin as the distance from the edge of the
    page to the bottom of the lowest mark/object rendered on the
    page."
    :initform (m:mm 1 :cm))
   (bubble-radius
    :accessor p-bubble-radius
    :documentation "A measure. The radius of all bubbles on this page."
    :initform (m:mm 0.25 :cm))
   (bubble-row-height
    :accessor p-bubble-row-height
    :documentation "A measure. The vertical distance from the midpoint
    of a bubble in one row to the midpoint of a bubble directly below
    in a succeeding row."
    ;; 2014-10-20: 1 cm is acceptable
    ;; 2014-10-21: try 0.8 cm
    :initform (m:mm 0.8 :cm))
   (bubble-rows-cal-mark-radius
    :accessor p-bubble-rows-cal-mark-radius
    :documentation "A measure."
    :initform (m:mm 0.2 :cm))
   (bubble-rows-post-cal-mark-gap-height
    :accessor p-bubble-rows-post-cal-mark-gap-height
    :documentation "A measure."
    :initform (m:mm 0.2 :cm))
   (bubble-rows-pre-gap-height
    :accessor p-bubble-rows-pre-gap-height
    :documentation "A measure. The minimum vertical distance which
    should precede the set of bubble rows on the page."
    :initform (m:mm 0.5 :cm))
   (bubble-rows-post-gap-height
    :accessor p-bubble-rows-post-gap-height
    :documentation "A measure. The minimum vertical distance which
    should succeed the set of bubble rows on the page."
    :initform (m:mm 0.5 :cm))
   (bottom-box-height
    :accessor p-bottom-box-height
    :documentation "The height of the box which sits with its bottom
    edge directly above the lower calibration marks of the page. This
    box excludes bubble rows and is reserved for text markup, a
    barcode, or other arbitrary markup."
    :initform (m:mm 2 :cm))
   (cal-mark-radius
    :accessor p-cal-mark-radius
    :documentation "A measure."
    :initform (m:mm 0.4 :cm))
   ;; CAL-MARK-UL: populated with x,y coordinates only when coordinates are established
   (cal-mark-ul
    :accessor p-cal-mark-ul
    :documentation "xy coordinate of center of upper-left calibration mark"
    :initform nil)
   (cal-mark-ur
    :accessor p-cal-mark-ur
    :documentation "xy coordinate of center of upper-right calibration mark"
    :initform nil)
   (cal-mark-ll
    :accessor p-cal-mark-ll
    :documentation "xy coordinate of center of lower left calibration mark"
    :initform nil)
   (cal-mark-lr
    :accessor p-cal-mark-lr
    :documentation "xy coordinate of center of lower right calibration mark"
    :initform nil)
   (cal-mark-x
    :accessor p-cal-mark-x
    :documentation "x coordinate of center(s) of left calibration mark(s)."
    :initform (m:mm 1.5 :cm))
   (cal-mark-y
    :accessor p-cal-mark-y
    :documentation "y coordinate of center(s) of upper calibration mark(s)."
    :initform (m:mm -1.5 :cm))
   ;; 2014-10-20: 9 cm clearly works fine
   ;; 2014-10-21: try 8 cm
   (id-box-height
    :accessor p-id-box-height
    :documentation "Vertical distance reserved for the id box (only on
    a page with an id box)."
    :initform (m:mm 8 :cm))
   (left-margin
    :accessor p-left-margin
    :documentation "A measure representing the left margin of the page."
    :initform (m:mm 2 :cm))
   (paper-size
    :accessor p-paper-size
    :documentation "A CLPS paper size object."
    :initform (clps::nickname->paper-size :letter))
   (top-margin
    :accessor page-top-margin
    :documentation "A measure representing the top margin of the page."
    :initform (m:mm 1.5 :cm))
   (top-textbox-height
    :accessor p-top-textbox-height
    :documentation "A measure representing the height of the box of
    text at the top of the page."
    :initform (m:mm 2 :cm))))

;; ID describes the bubble rows used for specifying an identifier
(defclass id ()
  (
   ;; 2014-10-20: 9 cm clearly works fine
   ;; 2014-10-21: try 8 cm
   (box-height
    :accessor id-box-height
    :documentation "Vertical distance reserved for the id box (only on
    a page with an id box)."
    :initform (m:mm 8 :cm))
   (row-count
    :accessor id-row-count
    :documentation "An integer. The number of prompts -- i.e., the number of rows used to specify the identifier."
    :initarg :row-count
    :initform 6)
   (row-chars
    :accessor id-row-chars
    :documentation "A sequence of characters. The characters used in the bubbles for a row."
    :initarg :row-chars
    :initform *id-row-bubble-labels*)))

(defparameter *page*
  (make-instance 'page))

;;
;; functions
;;
(defun makesheets (out &key
                         ;; answers-annotations: short annotations to place directly adjacent to bubble row; either nil or '("1" "fork" "glurp")
                         answers-annotations
                         ;; answers-fills: patterns for bubble fills (integers represent positions of bubbles to be filled); either NIL or '((1 3) NIL (1 2) (0))
                         answers-fills
                         barcode
                         id
                         (json-path t)
                         instructions
                         (number-questions-p t) (page *page*)
                         questions-bubble-rows
                         (title "")
                         xy-path
                         )
  "Generate TikZ markup for a set of bubble sheets. Return value is undefined. Send the markup to the destination specified by OUT (a path, a stream, or a string representing a path). ID is an id object. INSTRUCTIONS is either NIL or a funcallable entity which accepts a single argument, an integer corresponding to the page number, and returns a string (presumably corresponding to the set of instructions for the corresponding page). BARCODE is either NIL or a funcallable entity which accepts a single argument, an integer corresponding to the page number, and returns a path object -- which points to a PDF graphic which should be included in the document at an appropriate location. PAGE is a page object containing the default dimensions for the various components of each page. TITLE is a string or a path. If XY-PATH is true, generate an xy (bubble-coordinate) file. If XY-PATH is a pathname, generate the xy file at the specified location. Otherwise use the default location.

QUESTIONS-BUBBLE-ROWS specifies, for each question in the test, the
composition of the corresponding bubble row. If QUESTIONS-BUBBLE-ROWS
is an integer, that number specifies the total number of questions. In
this case, each bubble row has the default number of bubbles and the
default choice designators. If QUESTIONS-BUBBLE-ROWS is a list, each
member corresponds to a question. The length of the list specifies the
total number of questions. Each list member is either NIL or a list of
chars. NIL indicates a question/item for which a set of bubbles can't
be used as a response. A list of chars is treated as an ordered set
specifying the choice designators for the bubble row."
  (declare
           (page page))
  (log:debug answers-annotations answers-fills questions-bubble-rows barcode
             id
             xy-path)
  (cond ((funcallablep instructions)
         t)
        ((stringp instructions)
         t)
        ((pathnamep instructions)
         (setf instructions (dfile:file-to-string instructions))))
  (if (and title
           (not (stringp title)))
      (setf title (dfile:file-to-string title)))
  (let ((bubbles-obj (make-instance 'bubbles))
        (id-obj (or id (make-instance 'id))))
    (makesheets* out
		 id-obj
		 number-questions-p page
                 bubbles-obj
                 answers-annotations
		 answers-fills
		 questions-bubble-rows
		 barcode
		 instructions
		 title
		 xy-path)
    ;; At this point, all pages are considered equal in terms of PAGE;
    ;; calibration marks are in place --> dump page data
    (progn
      (if (stringp out)
          (setf out (pathname out)))
      (let ((json-pathspec (cond (json-path
                                  json-path)
                                 ((pathnamep out)
                                  (make-pathname :type "json"
                                                 :defaults out))
                                 (t
                                  (dfile::tempfile :suffix "json")))))
      (when json-pathspec
        (with-open-file (json-stream json-pathspec
                                     :direction :output
                                     :if-does-not-exist :create
                                     :if-exists :supersede)
          (write-bubbles-json id-obj page json-stream :bubbles bubbles-obj)))
      (values page id-obj bubbles-obj json-pathspec))))

(defun answer-rows (flat-answer-spec stream y
                    &key answers-annotations answers-fills
                      bubble-coordinate-stream bubbles
                      number-questions-p page)
  "FLAT-ANSWER-SPEC is documented at the docstring for BUBBLE-ROWS."
  (declare (stream stream))
  ;; sanity check(s)
  (assert (or (integerp flat-answer-spec)
              (list-of? flat-answer-spec 'list)))
  (assert (m:measurep y))
  (dltx:comment "questions/answers bubbles/rows" :stream stream)
  (bubble-rows flat-answer-spec stream y
               :answers-annotations answers-annotations
	       :answers-fills answers-fills
               :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
               :char-choice-designators *letter-single-char-answer-designators* ; disregarded unless FLAT-ANSWER-SPEC is NIL
               :node-label "Anode"
               :number-questions-p number-questions-p
               :page page))

(defun barcode (page stream string)
  "Barcode encodes string STRING"
  (declare (string string))
  ;; note: CALIBRATION-MARKS makes same calculation
  (let ((y
         (m:m- (m:mm 0 :cm)                             ; assume y of top node
               (m:m- (clps:paper-size-length (p-paper-size page))
                     (page-top-margin page)
                     (page-bottom-margin page)))))
       (format stream "
% date-page number-total page count
\\node at (3.25in,~A) { 
" (m:m->string y))
       (dltx:l "barcode" string :s stream)
       (write-string " };
"  stream)))

;; NODE-LABEL is a string corresponding to the reference node for the set of rows
(defun bubble-rows (flat-answer-spec stream y
                    &key answers-annotations answers-fills
                      bubble-coordinate-stream bubbles
                      char-choice-designators
                      node-label number-questions-p page)
  "FLAT-ANSWER-SPEC is either a list or an integer. If an integer,
  this specifies the number of prompts. If a list, it is not organized
  by questionset (it is flattened once relative to …). If
  FLAT-ANSWER-SPEC is a list, each member is a list of strings, each
  string corresponding to the label for a bubble for the corresponding
  prompt/question/choice/selection. [ what is the 'label' used for?
  how is it different than a char-choice-designator? ]

Y is a measure representing the y coordinate.

CHAR-CHOICE-DESIGNATORS is a list of characters suitable for bubble
interiors and for designating appropriate choices (e.g., digits in an
id # or answers for a multiple-choice question) -- i.e., a list of
single character objects used for designating choices. These specify
the default characters to be used inside each bubble if
FLAT-ANSWER-SPEC is an integer. Subsets of this set are used based on
the # of answers for a given question. Note that, if relying on
defaults, this set, or subsets thereof, should be used when rendering
answers for multiple-choice questions."
  (declare (stream stream))
  ;;(log:debug flat-answer-spec)
  ;; sanity check(s)
  (assert (or (not char-choice-designators)
              (list-of? char-choice-designators 'character)))
  (assert (or (integerp flat-answer-spec)
              (list-of? flat-answer-spec 'list)))
  (assert (m:measurep y))
  ;; optional calibration/reference mark (node not used since radius precisely specified -- however, need a node for other tikz commands to reference)
  ;; (format stream "\\draw (~A,~A) [fill=blue,anchor=center] circle (~A);"
  ;;      (m:m->string (p-left-margin page))
  ;;      (m:m->string y)
  ;;      (m:m->string (p-bubble-rows-cal-mark-radius page)))
  (let ((x (p-left-margin page))
        (y (m:m- y
                 (p-bubble-rows-pre-gap-height page)
                 (p-bubble-rows-cal-mark-radius page))))
   (tkz:node (m:m->string x) ; "0cm"
             (m:m->string y)
             :fill nil
             :minimum-height nil
             :name node-label
             :shape nil
             :stream stream)
    ;; grid (development)
    ;; (if gridp
    ;;     (format stream "\\draw[step=0.25cm, color=gray] (IDnode) grid ++(10cm,~A);" (m:m->string y)))
    ;; visual of reference node for bubble rows (development)
    ;; (format stream "\\draw (~A,~A) [fill=blue,anchor=center] circle (0.3cm);" "1cm" (m:m->string y))
    
   ;; using relative node makes it more difficult to output exact x y coordinates of bubbles for the use of other programs
   ;;(bubble-rows-rows-relative …)
    (bubble-rows-rows-absolute flat-answer-spec stream y
                               char-choice-designators
                               number-questions-p page
                               :answers-annotations answers-annotations
                               :answers-fills answers-fills
                               :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles)))

;; this doesn't use a reference node
(defun bubble-rows-rows-absolute (flat-answer-spec stream
                                  initial-y
                                  char-choice-designators
                                  number-questions-p page
                                  &key answers-annotations answers-fills
                                    bubble-coordinate-stream bubbles)
  "INITIAL-Y is a measure. See the BUBBLE-ROWS docstring for more on
CHAR-CHOICE-DESIGNATORS."
  (declare (stream stream))
  ;;(log:debug flat-answer-spec)
  ;; Q: Is FLAT-ANSWER-SPEC as integer ever used? A: YES
  (let ((n (if (integerp flat-answer-spec)
               flat-answer-spec
               (length flat-answer-spec)))
        (row-height (p-bubble-row-height page))
        ;; per-row content y offset: put things in center of row
        (row-y-offset (m:m/ (p-bubble-row-height page) 2)))
    (dotimes (row-n n)
      ;; a single row: row marker followed by bubbles
      ;; - row is 1 cm high
      ;; - marker(s)/bubbles are centered in row
      ;; - marker(s)/bubbles are spaced 0.75cm center-to-center
      (let ((y (m::m- initial-y
                      row-y-offset
                      (m::m* row-height row-n))))
        ;; % row marker, a filled square, centered on 0
        (let ((square-width (m:mm 0.4 :cm)))
          (format stream "\\draw [fill=black] ++(-0.2cm,~A) rectangle ++(~A,-~A) {};"
                  (m:m->string (m::m+ y (m::m/ square-width 2)))
                  (m:m->string square-width)
                  (m:m->string square-width)))
        ;; response/question number
        ;; - located in gap between row marker and first bubble
        (when number-questions-p
          (tkz::node "1cm"
                     (m:m->string y)
                     :relxy "++"
                     :stream stream
                     :text (format nil "~S."
                                   (+ (or (if (integerp number-questions-p)
                                              number-questions-p
                                              nil)
                                          ;; fall back to row number +1 (unlikely most respondents will find it natural to have a form where question numbering begins w/0)
                                          1)
                                      row-n))))
        ;; % bubbles
        (let ((bubble-labels
                (response-spec-to-choice-designators
                 ;; FLAT-ANSWER-SPEC: (NIL ("a" "b" "c" "d" "e"))
                 (when (consp flat-answer-spec)
                   (nth row-n flat-answer-spec)) ; <-- the response-spec for the row
                 char-choice-designators ; defaults
                 ))
              (fill-pattern (nth row-n answers-fills))
              (annotation (nth row-n answers-annotations)))
          (let ((bubble-labels-length (length bubble-labels))
                (y-string (m:m->string y))
                (marker-bubble-x-gap (m:mm 1 :cm)))
            (dotimes (i bubble-labels-length)
              ;; 0.75: no gap between marker and first bubble
              ;; 1.50: gap between marker and bubble
              (let ((x (m:m+ (m::mm 0.75 :cm)
                             marker-bubble-x-gap
                             (m:mm (* 0.75 i) :cm))))
                (let ((x-string (m:m->string x)))
                  (tkz:node x-string y-string
                            :relxy "++"
                            :text-color "gray"
                            :stream stream
                            :text (string (nth i bubble-labels)))
                  ;; % bubble circles
                  (if (member i fill-pattern)
                      (write-string "\\draw [fill=black,opacity=1,thick] " stream)
                      (write-string "\\draw [draw opacity=1,thick] " stream))
                  (write-string "++" stream)
                  ;; (TKZ:COORDINATE ...)
                  (write-char #\( stream)
                  (write-string x-string stream)
                  (write-char #\, stream)
                  (write-string y-string stream)
                  (write-char #\) stream)
                  (write-string " circle (" stream)
                  (write-string (m:m->string (p-bubble-radius page)) stream)
                  (write-string ");
" stream)
                  ;; record bubble coordinates
                  (progn
                    (format bubble-coordinate-stream "~A ~A "
                            (m::m-numb x)
                            (m::m-numb y))
                    (vector-push-extend (m::m-numb x) (bubbles-coordinates bubbles))
                    (vector-push-extend (m::m-numb y) (bubbles-coordinates bubbles))))))
            (if annotation
                (tkz:node
                 ;; x-string
                 (m:m->string (m:m+ (m::mm 0.75 :cm)
                                    marker-bubble-x-gap
                                    (m:mm (* 0.75 bubble-labels-length) :cm)))
                 y-string
                 :relxy "++"
                 :text-color "gray"
                 :stream stream
                 :text (coerce-to-string annotation))))
          ;; Is it desirable to add a #\Newline (introduce a blank line) for non-multiple-choice/TF answers?
	  ;; Possibilities:
          ;;   1. NO  - reserve newline to indicate end of page
          ;;   2. YES - (why? if yes, probably best to have some other way of indicating a non-mc/tf answer -- something that is distinct from what is used to represent a page break (newline)
          ;; Currently:
	  ;;   #1 (NO)
          (when (> (length bubble-labels) 0)
            (write-char #\Newline bubble-coordinate-stream)
            (vector-push-extend nil    ; FIXME: how should end of row be represented? null? false? string?
                                (bubbles-coordinates bubbles))
            ;; (push :nnn                  ; what is a good fit here? (transition to lists?
            ;;       (bubbles-coordinates bubbles))
            )
          )))))

;; 8.5" = 21.59 cm
(defun calibration-marks (page s)
  "S is the output stream."
  (declare (page page))
  (dltx:comment "calibration marks and bar code" :stream s)
  (let ((sheet-width-cm (m::convert-units (clps:paper-size-width (p-paper-size page)) :cm))
        (sheet-length-cm (m::convert-units (clps:paper-size-length (p-paper-size page)) :cm)))
      (let ((cal-mark-radius-string (m:m->string (p-cal-mark-radius page)))
            ;; calibration marks are positioned absolutely irrespective of margins, etc.
            (x-right (m:m-
                      sheet-width-cm
                      ;; 1.5cm isn't far enough from edge for some
                      ;; printers (they will 'cut off' part of the
                      ;; circle)
                      (m:mm 2.5 :cm)))
            ;; note: BARCODE makes same calculation
            (y-bottom (m:m- (m:mm 1.5 :cm)      ; assume y of top node
                            ;; LaTeX can't handle mm as units -> convert to cm
                            sheet-length-cm
                            ;; FIXME: this unit conversion is awkward - encapsulate as a measures.lisp utility fn MM:CONVERT-UNITS
                            ;; (m:mm
                            ;; (unit-formulas:convert-unit
                            ;;  (list (m::m-numb
                            ;;          (clps:paper-size-length (p-paper-size page)))
                            ;;        (make-symbol (string (m::m-unit
                            ;;                               (clps:paper-size-length (p-paper-size page)))))

                            
                            ;;        )
                            ;;  'cm)
                            ;; :cm)

                            ;; (m:m- (clps:paper-size-length (p-paper-size page))
                            ;;               ;(page-top-margin page)
                            ;;               ;(page-bottom-margin page)
                            ;;               )
                            )))
        ;; set marks
        (progn
          ;; upper left mark
          (setf (p-cal-mark-ul page) (list (p-cal-mark-x page)
                                           (p-cal-mark-y page)))
          ;; upper right mark
          (setf (p-cal-mark-ur page) (list x-right
                                           (p-cal-mark-y page)))
          ;; lower left mark
          (setf (p-cal-mark-ll page) (list (p-cal-mark-x page)
                                           y-bottom))
          ;; lower right mark
          (setf (p-cal-mark-lr page) (list x-right
                                           y-bottom)))
        (format s
                "
\\draw[calmark] (~A,~A) circle (~A);
\\draw[calmark] (~A,~A) circle (~A);
\\draw[calmark] (~A,~A) circle (~A);
\\draw[calmark] (~A,~A) circle (~A);
"
                ;; upper left mark
                (m:m->string (first (p-cal-mark-ul page)))
                (m:m->string (second (p-cal-mark-ul page)))
                cal-mark-radius-string
                ;; upper right mark
                (m:m->string (first (p-cal-mark-ur page)))
                (m:m->string (second (p-cal-mark-ur page)))
                cal-mark-radius-string
                ;;(m:m->string x-right) (m:m->string (p-cal-mark-y page)) cal-mark-radius-string
                
                ;; lower left mark
                (m:m->string (first (p-cal-mark-ll page)))
                (m:m->string (second (p-cal-mark-ll page)))
                cal-mark-radius-string
                ;;(m:m->string (p-cal-mark-x page)) (m:m->string y-bottom) cal-mark-radius-string

                ;; lower right mark
                (m:m->string (first (p-cal-mark-lr page)))
                (m:m->string (second (p-cal-mark-lr page)))
                cal-mark-radius-string
                ;;(m:m->string x-right) (m:m->string y-bottom) cal-mark-radius-string
                )
        ;; (log:debug sheet-width-cm
        ;;         sheet-length-cm
        ;;         (p-cal-mark-x page)
        ;;         x-right
        ;;         (p-cal-mark-y page)
        ;;         y-bottom)
        ;; (values sheet-width-cm
        ;;      sheet-length-cm
        ;;      x-right
        ;;      y-bottom
        ;;      )
        )))

(defun id-box (id stream y &key bubble-coordinate-stream bubbles page)
  (declare (stream stream))
  "N is the number of prompts."
  (dltx:comment "identification number bubbles/rows" :stream stream)
  (bubble-rows (id-row-count id) stream y
               :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
               :char-choice-designators (id-row-chars id)
               :node-label "IDnode" :page page))

;; If the pathname ID-PATH is a complete pathname specifying a path of
;; type 'id', then that pathname will be used for output. Otherwise,
;; the directory of ID-PATH will be used as the directory in which to
;; write foo.id (the file containing the parameters for the id box --
;; at this point, an integer ID-ROW-N indicating the number of rows
;; and ID-ROW-LABELS, a list indicating the values/labels for each
;; bubble in a row
(defun log-id-box-parameters (id id-path)
  (with-open-file (s
                   (if (equal "id" (pathname-type id-path))
                       id-path
                       (make-pathname :defaults id-path :type "id"))
                   :direction :output
                   :if-exists :supersede
                   :if-does-not-exist :create)
    (write (id-row-count id) :stream s)
    (write-char #\Space s)
    (dolist (char (id-row-chars id))
      (write-char char s))))

;; TEXT-BOXES-HEIGHT - height of text boxes above bubble rows (id and answer rows)
;; ANSWERS-SPEC: see documentation at MAKESHEETS*
;; ANSWERS-SPEC-I indicates the current position within FLAT-ANSWERS-SPEC if ANSWERS-SPEC is a list
;; FLAT-ANSWER-SPEC: see documentation at bubble-rows-rows-absolute
(defun make-sheet-given-heights (page text-boxes-height id number-questions-p flat-answer-spec s &key (answers-spec-i 0) bubble-coordinate-stream instructions page-number title)
  "Return an integer representing the number of questions/response
fields which were generated on this sheet."
  (declare (page page))
  ;; sanity checks
  (assert (or (list-of? flat-answer-spec 'list)
              (typep flat-answer-spec 'integer)))
  ;; calculate maximum # of questions sheet can hold
  (let ((cal-marks-total-height (m:m* (p-cal-mark-radius page) 4))
        ;; currently use fixed height id box
        (id-box-height (if (integerp id)
                           (m:mm 9 :cm)
                           (m:mm 0 :cm))))
    ;; MAX-SHEET-QUESTIONS-N: integer indicating maximum number of
    ;; questions which can fit on the current sheet
    (let ((max-sheet-questions-n
           (truncate (m:m/ (m:m-
                            (clps::paper-size-length (p-paper-size page))
                            (m:m+ cal-marks-total-height
                                  id-box-height
                                  (page-top-margin page)
                                  (p-bubble-rows-post-gap-height page)
                                  (page-bottom-margin page)
                                  text-boxes-height))
                           (p-bubble-row-height page))))
          ;; REQUEST-QUESTIONS-N: the number of questions we're
          ;; requested to render, if possible
          (request-questions-n (if (integerp flat-answer-spec)
                                   flat-answer-spec
                                   (length flat-answer-spec))))
      (let ((actual-sheet-questions-n (min request-questions-n
                                           max-sheet-questions-n)))
        ;; calculate question count remaining:
        ;; - need to know:
        ;;     question/item/response row height
        ;;     id box height
        ;;     top/bottom paper margins
        ;;     height of any other text boxes on page
        (makesheet-tikzpicture s
                               (if (integerp flat-answer-spec)
                                   actual-sheet-questions-n
                                   (subseq flat-answer-spec answers-spec-i (+ actual-sheet-questions-n answers-spec-i)))
                               page-number
                               :bubble-coordinate-stream bubble-coordinate-stream
                               :id id
                               :instructions instructions
                               :number-questions-p number-questions-p
                               :page page
                               :text-boxes-height text-boxes-height
                               :title title)
        actual-sheet-questions-n))))

;; NUMBER-QUESTIONS-P: if true, each bubble row is preceded by a number in the markup (i.e., questions are numbered) -- if an integer, use it as the first number in the numbering scheme (increment by 1 for each subsequent item)
(defun make-sheet-given-heights-TEST (page text-boxes-height
				      id
				      number-questions-p
                                      flat-answer-spec s page-number
                                      &key answers-annotations answers-fills
                                        barcode bubble-coordinate-stream bubbles
                                        instructions title)
  "Return an integer representing the number of questions/response
fields which were generated on this sheet."
  (declare (integer page-number))
  ;; calculate maximum # of questions sheet can hold
  (let ((cal-marks-total-height (m:m* (p-cal-mark-radius page) 4))
        ;; currently use fixed height id box
        (id-box-height (if id
                           (id-box-height id)
                           (m:mm 0 :cm))))
    ;; MAX-SHEET-QUESTIONS-N: integer; maximum number of questions
    ;; which can fit on the current sheet
    (let ((max-sheet-questions-n
           (truncate (m:m/ (m:m-
                            (clps::paper-size-length (p-paper-size page))
                            (m:m+ cal-marks-total-height
                                  id-box-height
                                  (page-top-margin page)
                                  (p-bubble-rows-post-gap-height page)
                                  (p-bottom-box-height page)
                                  (page-bottom-margin page)
                                  text-boxes-height))
                           (p-bubble-row-height page))))
          ;; REQUEST-QUESTIONS-N: the number of questions we're
          ;; requested to render, if possible
          (request-questions-n (if (integerp flat-answer-spec)
                                   flat-answer-spec
                                   (length flat-answer-spec))))
      (let ((actual-sheet-questions-n (min request-questions-n
					   max-sheet-questions-n)))
        ;; calculate question count remaining:
        ;; - need to know:
        ;;     question/item/response row height
        ;;     id box height
        ;;     top/bottom paper margins
        ;;     height of any other text boxes on page
        (makesheet-tikzpicture s
                               (if (integerp flat-answer-spec)
                                   actual-sheet-questions-n
                                   (subseq flat-answer-spec
					   0 actual-sheet-questions-n))
                               page-number
                               :answers-annotations answers-annotations
			       :answers-fills answers-fills
                               :barcode barcode
                               :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
			       :id id
                               :instructions instructions
                               :number-questions-p number-questions-p
                               :page page
                               :text-boxes-height text-boxes-height
                               :title title)
        actual-sheet-questions-n))))

(defgeneric makesheets* (out
                         id
                         number-questions-p page
                         bubbles
                         answers-annotations answers-fills answers-spec
                         barcode
                         instructions title xy-path)
  (:documentation "OUT is a pathname, a stream, or a namestring (a string representation of a  file name)."))

(defmethod makesheets* :before (out id number-questions-p page
                                bubbles
                                answers-annotations answers-fills answers-spec
                                barcode
                                instructions title xy-path)
  (declare (page page)
           (string title))
  ;; sanity checks
  (assert (or (integerp answers-spec)
              (list-of? answers-spec 'list))))

(defmethod makesheets* ((out pathname) id number-questions-p page
                        bubbles
                        answers-annotations answers-fills answers-spec
                        barcode
                        instructions title xy-path)
  (with-open-file (s
                   out
                   :direction :output
                   :if-exists :supersede
                   :if-does-not-exist :create)
    (if xy-path
        (let ((final-xy-path (if (pathnamep xy-path)
				 xy-path
				 (make-pathname :defaults out :type "xy"))))
          ;; prepare stream for logging button locations (log in an 'xy' file)
	  (with-open-file (bubble-coordinate-stream
                           final-xy-path
                           :direction :output
                           :if-exists :supersede
                           :if-does-not-exist :create)
            (makesheets-with-stream s id number-questions-p page answers-spec instructions title
                                    :answers-annotations answers-annotations
                                    :answers-fills answers-fills
                                    :barcode barcode
                                    :bubbles bubbles
                                    :bubble-coordinate-stream bubble-coordinate-stream)))
        (makesheets* s id number-questions-p page
                     bubbles
                     answers-annotations answers-fills answers-spec
                     barcode
                     instructions title xy-path))))

(defmethod makesheets* ((out stream) id number-questions-p page
                        bubbles
                        answers-annotations answers-fills answers-spec
                        barcode
                        instructions title xy-path)
  (makesheets-with-stream out id number-questions-p page answers-spec instructions title
                          :answers-annotations answers-annotations
                          :answers-fills answers-fills
                          :barcode barcode
                          :bubbles bubbles))

(defmethod makesheets* ((out string) id number-questions-p page
                        bubbles
                        answers-annotations answers-fills answers-spec
                        barcode
                        instructions title xy-path)
  (makesheets* (pathname out) id number-questions-p page
               bubbles
               answers-annotations
               answers-fills
               answers-spec
               barcode
               instructions title xy-path))

(defun makesheets-document (stream answers-spec
                            &key answers-annotations answers-fills
                              barcode bubble-coordinate-stream bubbles
                              id number-questions-p page
                              instructions title)
  (declare (optimize (speed 0) (space 0) (debug 3)))
  ;; sanity checks
  (assert (or (integerp answers-spec)
              (list-of? answers-spec 'list)))
  (dltx:document
   #'(lambda (doc-s)
       ;; define margins - this is essential for ensuring that tikz absolute coordinate 0,0 corresponds to the physical corner of the page
       (dltx:l "newgeometry"
               ;; make the entire page available; handle margins, etc.
               ;; internally (within DUBL)
               (format nil "top=~A, bottom=~A, left=0mm, right=0mm" "0cm" "0cm")
               :s doc-s)
       (makesheets-pages doc-s
                         answers-spec
                         :answers-annotations answers-annotations
                         :answers-fills answers-fills
                         :barcode barcode
                         :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
                         :id id
                         :instructions instructions
                         :number-questions-p number-questions-p
                         :page page
                         :title title))
   :stream stream))

(defun makesheets-pages (s flat-answer-spec &key answers-annotations answers-fills
                                              barcode bubble-coordinate-stream bubbles
                                              id instructions number-questions-p
                                              page
                                              title)
  (declare (optimize (speed 0) (space 0) (debug 3)))
  ;; sanity checks
  (assert (or (integerp flat-answer-spec)
              (list-of? flat-answer-spec 'list)))
  (multiple-value-bind (first-sheet-questions-n text-boxes-height)
      (makesheets-pages-firstpage s flat-answer-spec
                                  :answers-annotations answers-annotations
                                  :answers-fills answers-fills
                                  :barcode barcode
                                  :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
                                  :id id :number-questions-p number-questions-p :page page
                                  :instructions instructions :title title)
    (declare (ignore text-boxes-height))
    (when (> (length flat-answer-spec) first-sheet-questions-n)
      (makesheets-pages-rest02
       s
       (subseq flat-answer-spec first-sheet-questions-n)
       (if number-questions-p (1+ first-sheet-questions-n))
       :answers-annotations (if answers-annotations (subseq answers-annotations first-sheet-questions-n))
       :answers-fills (if answers-fills (subseq answers-fills first-sheet-questions-n))
       :barcode barcode
       :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
       :instructions instructions
       :page page))))

(defun makesheets-pages-firstpage (s flat-answer-spec
                                   &key answers-annotations answers-fills
                                     barcode bubble-coordinate-stream bubbles
                                     id number-questions-p page
                                     instructions
                                     title)
  "FLAT-ANSWER-SPEC - a list of lists OR an integer. FLAT-ANSWER-SPEC: see documentation at bubble-rows-rows-absolute"
  (declare (optimize (speed 0) (space 0) (debug 3)))
  ;; sanity check(s)
  (assert (or (list-of? flat-answer-spec 'list)
              (typep flat-answer-spec 'integer)))
  (let (;(instructions-height-allowance (m:mm 6 :cm))
        (title-height-allowance (m:mm 3 :cm)))
    ;; specify vertical space consumed by select page components
    (let (
          ;; specified in
          ;;(id-box-height (if id-n (m:mm 11 :cm)))
         
          ;; TEXT-BOXES-HEIGHT: total height of simple text boxes which are stacked above bubble rows (see above)
          (text-boxes-height
           (m:m+ title-height-allowance
                                        ;instructions-height-allowance
                 )))
      ;; FIRST-SHEET-QUESTIONS-N: # of questions actually handled on the first sheet
      (let ((first-sheet-questions-n
             (make-sheet-given-heights-TEST
              page
              text-boxes-height
              id number-questions-p
              flat-answer-spec
              s
              0
              :answers-annotations answers-annotations :answers-fills answers-fills
              :barcode barcode
              :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
              :instructions instructions :title title)))
        (values first-sheet-questions-n text-boxes-height)))))

;; FLAT-ANSWER-SPEC -- [remaining] answers to be handled by this function
;; NUMBER-QUESTIONS-P -- if non-nil, the integer to begin numbering questions with
(defun makesheets-pages-rest02 (s flat-answer-spec number-questions-p &key answers-annotations answers-fills
                                                                        barcode bubble-coordinate-stream instructions page)
    ;; sanity checks
  (assert (or (list-of? flat-answer-spec 'list)
              (typep flat-answer-spec 'integer)))
  (let ((questions-n (if (integerp flat-answer-spec)
                                   flat-answer-spec
                                   (length flat-answer-spec))))
    ;; as we iterate across answers, we track two indices
    ;; 1. PAGE-NUMBER (an integer, count beginning at 0, corresponding to the physical page number)
    ;; 2. FLAT-ANSWER-SPEC-I: the index within FLAT-ANSWER-SPEC
    (let ((flat-answer-spec-i 0)
          ;; assume that MAKESHEETS-PAGES-FIRST handled first page and then handed things off to MAKESHEETS-PAGES-REST
          (page-number 1)
          (remaining-questions-n questions-n))
      (tagbody
         ;; (progn initial stuff...)
       makesheet
         (if (> remaining-questions-n 0)
             (progn
               (dltx:l "clearpage" nil :s s :suppress-newline-p nil)
               (multiple-value-bind (questions-handled-n)
                   (make-sheet-given-heights-TEST
                    page
                    (m:mm 0 :cm) ; text-boxes-height is currently 0 if we're not on the first page
                    nil		 ; id
                    number-questions-p
                    (if (integerp flat-answer-spec)
                        remaining-questions-n
                        (subseq flat-answer-spec flat-answer-spec-i))
                    s
                    page-number
                    :answers-annotations (if answers-annotations (subseq answers-annotations flat-answer-spec-i))
                    :answers-fills (if answers-fills (subseq answers-fills flat-answer-spec-i))
                    :barcode barcode
                    :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
                    :instructions instructions)
                 (declare (integer questions-handled-n))
                 (incf page-number)
                 (setf number-questions-p (+ number-questions-p questions-handled-n))
                 (setf remaining-questions-n
                       (- remaining-questions-n questions-handled-n))
                 (setf flat-answer-spec-i (+ flat-answer-spec-i questions-handled-n))
                 (go makesheet)))
             (go post))
       post
         ;;(progn final stuff post-answer-sheets...)
         ))))

(defun makesheets-preamble (s)
  "S is a stream."
  (declare (stream s))
  (dltx:documentclass "article" :stream s)
  ;; % suppress pagination
  (dltx:l "pagestyle" "empty" :s s)
  (write-string "\\evensidemargin=0cm" s)
  (write-string "\\evensidemargin=0cm" s)
  ;; \parindent influences absolute positioning (X coordinate) for both textpos and tikz
  (write-string "\\setlength{\\parindent}{0pt}" s)
  (makesheets-preamble-packages s)
  (tkz:top s :tikzlibs '("backgrounds" "positioning" "fit")))

(defun makesheets-preamble-packages (s)
  (declare (stream s))
  ;; textpos option showboxes -> show text boxes
  (dltx:usepackage "textpos" :opts '("absolute") :stream s)
  (write-string "\\setlength{\\TPHorizModule}{10mm}
\\setlength{\\TPVertModule}{\\TPHorizModule}
\\textblockorigin{0mm}{0mm}" s)
  (dltx:usepackage "geometry" :opts '("landscape=false,left=0cm,right=0cm,paper=letterpaper,pass") :stream s)

  ;; bar code support
  ;;(dltx:usepackage "makebarcode" :opts '("code=Code39,X=.5mm,ratio=2.25,H=1cm") :stream s)

  ;; important in determining whether bubble is filled in or not to have relatively light text inside the bubble
  (dltx:usepackage "xcolor" :stream s)
  (write-string "\\definecolor{lightgray}{RGB}{129,129,129}" s))

(defmethod makesheets-with-stream (stream id number-questions-p page answers-spec instructions title &key answers-annotations answers-fills barcode bubble-coordinate-stream bubbles)
  (makesheets-preamble stream)
  (makesheets-document stream answers-spec
                       :answers-annotations answers-annotations
                       :answers-fills answers-fills
                       :barcode barcode
                       :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
                       :id id
                       :number-questions-p number-questions-p
                       :page page
                       :instructions instructions :title title))

(defun makesheet-tikzpicture (stream flat-answer-spec
                              page-number
                              &key answers-annotations answers-fills
                                barcode
                                bubble-coordinate-stream bubbles
				id instructions
				number-questions-p
                                page
                                text-boxes-height title)
  "TEXT-BOXES-HEIGHT is vertical space consumed by text boxes at top
of the page (above bubble rows (id and/or answer rows)).
FLAT-ANSWER-SPEC is documented at the docstring for
BUBBLE-ROWS-ROWS-ABSOLUTE."
  (declare (integer page-number))
  ;; sanity check(s)
  (assert (or (list-of? flat-answer-spec 'list)
              (typep flat-answer-spec 'integer)))
  ;; ID-BOX-Y: formerly (m:mm -2 :cm)           ; initial y
  (let* ((id-box-y (m:m- (p-cal-mark-y page) ; y coordinate of upper left calibration mark
                         (p-cal-mark-radius page)
                         text-boxes-height))
         (answer-rows-y (if id
                            (m:m- id-box-y (id-box-height id))
                            id-box-y)))
    ;; title
    (when title
      (write-string "

%%
%% test title
%%

% textpos package provides textblock; 
% syntax: \begin{textblock}{hsize}(hpos,vpos)
% box is placed w/upper left-hand corner at hpos,vpos coordinates
% units are TPHorizModule and TPVertModule
\\begin{textblock}{8}(3,1)
" stream)
      (write-string title stream)
      (write-string "\\end{textblock}" stream))
    (when instructions
      ;; instructions
      ;; - put at fixed position to right of the ID box
      (format stream "
%%
%% test instructions, etc.
%%

\\begin{textblock}{~A}(~A,~A)
~A
\\end{textblock}

"
              "8"  ;; horizontal size
              "12" ; X position (textpos units are unitless)
              ;; textpos units are
              ;; (1) unitless; default is 1cm
              ;; (2) positive in descending page (tikz units, the units we use here, are - as they descend page)
              (* -1 (m::m-numb id-box-y))
              ;; INSTRUCTIONS
              (cond ((stringp instructions)
                     instructions)
                    ((funcallablep instructions)
                     (funcall instructions page-number))
                    (t
                     (error "instructions should either be a string or a funcallable object")))))
    (tkz:tikzpicture
     #'(lambda (s)
         (declare (ignore s))           ; just use STREAM
         (makesheet-tikzpicture-body
          stream
          answer-rows-y
          id-box-y
          flat-answer-spec                      ; questions-n
          page-number
          :answers-annotations answers-annotations
          :answers-fills answers-fills
          :barcode barcode
          :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
	  :id id
          :number-questions-p number-questions-p
          :page page))
     stream
     ;; tikz style (currently unused):
     ;; dot/.style={fill=blue,circle,minimum size=3pt},
     ;; shift={(current page.north west)} is essential in defining the tikzpicture origin (0,0) as a precise position on the physical page
     :opts '("remember picture" "overlay" "shift={(current page.north west)}" "
        % Styles
        calmark/.style={fill=black,circle}
        ")))
  ;; assume this represents the end of the page
  ;; use empty line to represent new page
  (write-char #\Newline bubble-coordinate-stream))

(defun makesheet-tikzpicture-body (stream answer-rows-y id-box-y
				   flat-answer-spec
                                   page-number
                                   &key answers-annotations answers-fills
                                     barcode bubble-coordinate-stream bubbles
                                     id number-questions-p page)
  "FLAT-ANSWER-SPEC is documented at the docstring for BUBBLE-ROWS."
  (declare (integer page-number)
           (stream stream))
  ;; (log:debug flat-answer-spec
  ;;            page-number
  ;;            answers-annotations
  ;;            answers-fills)
  ;; sanity check(s)
  (assert (or (list-of? flat-answer-spec 'list)
              (typep flat-answer-spec 'integer)))
  (dltx:l "begin" "scope" :s stream)
  ;; development only: reference node at very upper-left corner of page
  ;;   (write-string "%% reference node for upper-left page corner
  ;; \\draw[anchor=center,fill=red] (0cm,0cm) circle (0.3cm);" stream)
  (calibration-marks page stream)
  (when id
    (id-box id
	    stream
            id-box-y
            :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
            :page page))
  (answer-rows flat-answer-spec stream
               answer-rows-y
               :answers-annotations answers-annotations :answers-fills answers-fills
               :bubble-coordinate-stream bubble-coordinate-stream :bubbles bubbles
               :number-questions-p number-questions-p
               :page page)
  (progn
    (let ((sane-page-number-maximum 1000))
      (assert (< page-number sane-page-number-maximum))
      (when barcode
        (let ((barcode-path (funcall barcode page-number)))
          ;;(barcode page stream barcode-string)
          (let ((y
                  (m:m- (m:mm 0 :cm)                                ; assume y of top node
                        (m:m- (clps:paper-size-length (p-paper-size page))
                              (page-top-margin page)
                              (page-bottom-margin page))))
                (x "4.0in"))
            (format stream "
% date-page number-total page count
\\node at (~A,~A) { 
" x (m:m->string y))
         ;; \includegraphics[width=0.5\textwidth]{Example}
         (dltx:l "includegraphics"
                 (namestring barcode-path)
                 ;;:opts "width=0.5\\textwidth"
                 :s stream
                 )
         (write-string " };
"  stream))
       )))
  (dltx:l "end" "scope" :s stream))

;; every member of object-plists has the form (<key-as-string> . <object-plist)
;; example member: ("scoring" "correct-fill-patterns" ((1 3) NIL (1 2) (0)))
;; The above specifies key 'scoring' for an object with a single key, 'correct-fill-patterns', with a single value, ((1 3) NIL (1 2) (0))
(defun write-bubbles-json (id page stream &key bubbles object-plists)
  "Generate JSON with page, id, and other information regarding the
sheet(s) generated. The value for page key includes key/value pairs
specifying page dimensions and calibration marks based on the page
object PAGE."
  (declare (dubl:id id)
           (dubl:page page))
  ;; All JSON output is in cm. Thus, units can be implicit.
  (let ((page-plist (list
                     :object-plist
                     ;; page width
                     "width"
                     (m:m->string
                      (m::convert-units (clps:paper-size-width (p-paper-size page)) :cm)
                      :unitp nil)
                     ;; page height/length
                     "length"
                     (m:m->string
                      (m::convert-units (clps:paper-size-length (p-paper-size page)) :cm)
                      :unitp nil)
                     ;; calibration marks
                     "cal-mark-ul" (list
                                    (m:m->string (first (p-cal-mark-ul page)) :unitp nil)
                                    (m:m->string (second (p-cal-mark-ul page)) :unitp nil))
                     "cal-mark-ur" (list
                                    (m:m->string (first (p-cal-mark-ur page)) :unitp nil)
                                    (m:m->string (second (p-cal-mark-ur page)) :unitp nil))
                     "cal-mark-ll" (list
                                    (m:m->string (first (p-cal-mark-ll page)) :unitp nil)
                                    (m:m->string (second (p-cal-mark-ll page)) :unitp nil))
                     "cal-mark-lr" (list
                                    (m:m->string (first (p-cal-mark-lr page)) :unitp nil)
                                    (m:m->string (second (p-cal-mark-lr page)) :unitp nil))
                     "bubble-radius" (m:m->string
                                      (m::convert-units (p-bubble-radius page) :cm)
                                      :unitp nil)
                     ))
        (id-plist (list :object-plist
                        "box_height" (m:m->string
                                      (m::convert-units (id-box-height id) :cm)
                                      :unitp nil)
                        "row_count" (id-row-count id)
                        "row_chars" (id-row-chars id))))
    ;; keys-and-object-plists
    (let ((shasht-object-plist (list :object-plist
                                     "page" page-plist
                                     "id" id-plist
                                     "bubbles" (when bubbles (bubbles-coordinates bubbles))
                                     )))
      (when object-plists
        (setf shasht-object-plist (append shasht-object-plist
                                          object-plists)))
      (shasht:write-json shasht-object-plist
                         stream))))

(defun response-spec-to-choice-designators (response-spec
                                            default-char-choice-designators ; or use default single-character response designators provided by DUBL ?
                                            )
  "A RESPONSE-SPEC specifies possible responses, suitable for use in a set of bubbles, for a single item (e.g., a survey or test question). An RESPONSE-SPEC is one of

1. NIL                  -> use defaults
2. an integer           -> use defaults from first default through nth default (e.g., RESPONSE-SPEC = 4  ==>  (a) (b) (c) (d)  if defaults are '(#\a #\b #\c #\d #\e . . .)
3. a list of characters -> use these characters
"
  (cond
    ((not response-spec)
     default-char-choice-designators)
    ((integerp response-spec)
     (subseq default-char-choice-designators
             0 response-spec))
    ((list-of? response-spec 'character)
     response-spec)
    (t
     (error (format nil "Unanticipated RESPONSE-SPEC value ~S" response-spec))
     )))
