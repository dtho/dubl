(in-package :cl-user)

(defpackage :dubl
  (:use :cl)
  (:documentation
   "This program is released under the terms of the Lisp Lesser GNU
  Public License http://opensource.franz.com/preamble.html, also known
  as the LLGPL. Copyright: David A. Thompson.")
  (:export id
	   makesheets
           page
           write-bubbles-json))

