(in-package :dubl)

;; A BUBBLES object holds the locations of a set of bubbles.
(defclass bubbles ()
  ((coordinates
    :accessor bubbles-coordinates
    :documentation
    "A vector. Represents rows on a bubble sheet. Organized by
horizontal row. The end of a row is represented by NIL."
    :initform (make-array 2 :fill-pointer 0 :adjustable t))))
