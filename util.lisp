(in-package :dubl)

;; string.lisp
(defun coerce-to-string (a)
  "Return a string. If A is NIL, return an empty string. If A is any other non-string object, return 'string version' of A. If A is a string, return A."
  (the string
       (if a
	   (if (stringp a) 
	       a 
	       (write-to-string a))
	   "")))

;; function.lisp
(defun funcallablep (x)
  (member (type-of x) '(COMPILED-FUNCTION ; with newer SBCL versions
			function symbol))
  ;; how does SBCL defines the SB-KERNEL::callable type
  ;; exactly as one would expect: 
  ;; - in src/code/deftypes-for-target.lisp:
  ;;   '(or function symbol)
  ;;(typep x 'SB-KERNEL::callable)
  )

;; list.lisp
(defun list-of? (some-list type-specifier)
  "Return a true value if each member of SOME-LIST is an object of type specifier TYPE-SPECIFIER."
  ;; a type-specifier can be a symbol or a cons or a class 
  (declare (optimize (speed 3)) 
	   (list some-list))
  (block return-t
    (map nil #'(lambda (x)
		 (unless (typep x type-specifier) 
		   (return-from return-t nil)))
	 some-list)
    t))
