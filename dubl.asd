(defsystem :dubl
  :description "DAT bubble sheet generation"
  :serial t
  :components ((:file "packages")
	       (:file "util")		; DCU
               (:file "bubbles")
	       (:file "makesheets")
	       )
  :depends-on (:dfile
	       :paper-sizes
	       :measures
	       :shasht
	       :tikz))
